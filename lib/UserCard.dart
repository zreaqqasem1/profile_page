import 'package:flutter/material.dart';

class UserCard extends StatelessWidget {

  final String info ;
  final String info1;
  final IconButton icon1;
  final IconButton icon2;


  const UserCard({
    Key key, this.info, this.info1, this.icon1, this.icon2,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Column(
            children: [
              Card(
                color: Colors.white,
                child: Container(
                  width: ((MediaQuery.of(context).size.width)/2.1 )- 10,
                  height: 70,
                  child: Center(

                    child: icon1,
                  ),
                ),
              ),

              Text('$info',
                style: TextStyle(
                    color: Colors.white
                ),
              ),

            ],
          ),
          Spacer(),

          Column(
            children: [
              Card(
                color: Colors.white,
                child: Container(
                  width: ((MediaQuery.of(context).size.width)/2.1) - 10,
                  height: 70,
                  child: Center(

                    child: icon2,

                  ),
                ),
              ),

              Text('$info1',
                style: TextStyle(
                    color: Colors.white
                ),
              ),

            ],
          ),

        ],
      ),

    );
  }
}

// here is a class for a Icon


