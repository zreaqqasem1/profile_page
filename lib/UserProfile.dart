import 'package:flutter/material.dart';
import 'UserCard.dart';
import 'PasswordCard.dart';
import 'ProfilePictureWidget.dart';
class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Color(0xff363636),

        body:
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage('https://images.unsplash.com/photo-1555952517-2e8e729e0b44?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1300&q=80'),
              fit: BoxFit.cover,
            ),
          ),
          child:

          SingleChildScrollView(

            child:
            LayoutBuilder(
              builder: (context,constraint){
                return
                  Container(
                    margin: const EdgeInsets.only(top: 100),
                    height: MediaQuery.of(context).size.height,

                    child:
                    Padding(
                      padding: EdgeInsets.only(left: 10,right: 10),
                      child: Column(
                        children: <Widget>[
                          ProfilePictureWidget(),

                          Container(
                            height: 12,
                          ),

                          Text('Qasem Zreaq',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                color: Colors.white
                            ),
                          ),

                          Container(
                            height: 10,
                          ),

                          Text('24 Years',
                            style: TextStyle(
                                color: Colors.white
                            ),
                          ),
                          Container(

                            height: 5,
                          ),

                          Text('Ramallah',
                            style: TextStyle(
                                color: Colors.white
                            ),
                          ),

                          Container(
                            height: 25,
                          ),

                          PasswordCard(),

                          Text('Password',
                            style: TextStyle(
                                color: Colors.white
                            ),
                          ),
                          Container(
                            height: 15,
                          ),
                          UserCard(info: '03/11/1996',info1: 'zeraqqasem',icon1: IconButton(
                            icon: Icon(Icons.calendar_today),
                            color: Colors.red,
                            onPressed: () {

                              print('// Action Dont know about it  :/');

                            },
                          ),
                            icon2: IconButton(
                              icon: Icon(Icons.person),
                              color: Colors.lightGreen,
                              onPressed: () {

                                print('// Action Dont know about it  :/');

                              },
                            ),
                          ),

                          Container(
                            height: 10,
                          ),

                          UserCard(info: 'zreaqqasem@ymail.com',info1: 'Male',icon1: IconButton(
                            icon: Icon(Icons.mail),
                            color: Colors.orangeAccent,
                            onPressed: () {

                              print('// Action Dont know about it  :/');

                            },
                          ),
                            icon2: IconButton(
                              icon: Icon(Icons.people),
                              color: Colors.lightBlue,
                              onPressed: () {

                                print('// Action Dont know about it  :/');

                              },
                            ),
                          ),

                          Container(
                            height: 10,
                          ),

                          UserCard(info: 'interest',info1: 'Media',icon1: IconButton(
                            icon: Icon(Icons.accessibility),
                            color: Colors.lightGreen,
                            onPressed: () {

                              print('// Action Dont know about it  :/');

                            },
                          ),
                            icon2: IconButton(
                              icon: Icon(Icons.perm_media),
                              color: Colors.pink,
                              onPressed: () {

                                print('// Action Dont know about it  :/');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  );
              },
            ),
          ),
        ),


      ),

    );


  }
}
