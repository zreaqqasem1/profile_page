import 'package:flutter/material.dart';

class ProfilePictureWidget extends StatelessWidget {
  const ProfilePictureWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        CircleAvatar(
          radius: 45,
          backgroundColor: Colors.black12,
          backgroundImage: NetworkImage('https://images.unsplash.com/photo-1555952517-2e8e729e0b44?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1300&q=80'),
        ),
        Positioned(
          bottom: -8,
          right: 22.5,
          left: 22.5,
          child: Icon(
            Icons.verified_user,
            color: Colors.blue,
          ),
        ),
      ],
      overflow: Overflow.visible,
    );
  }
}